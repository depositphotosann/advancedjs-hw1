"use strict"

/*
1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

Прототипне наслідування в JavaScript - це механізм, за допомогою якого об'єкти можуть успадковувати властивості та методи від інших об'єктів. Кожен об'єкт у JavaScript має посилання на інший об'єкт, який називається прототипом. 
При зверненні до властивості чи методу об'єкту, який відсутній в самому об'єкті, JavaScript автоматично шукає цю властивість чи метод у його прототипі, і таким чином, успадковує їх.
Це може бути краще зрозуміло на прикладі. Наприклад, уявімо, що ми маємо об'єкт car, який має властивість color та метод drive(). Тепер, якщо ми створимо новий об'єкт myCar із car як прототипом, myCar також матиме доступ до властивості color та методу drive(), навіть якщо вони не вказані в самому об'єкті myCar.
Отже, прототипне наслідування дозволяє створювати ієрархію об'єктів, де один об'єкт може успадковувати властивості та методи іншого. Це дозволяє підтримувати код зрозумілим та організованим, а також забезпечує гнучкість при розробці програм.

2. Для чого потрібно викликати super() у конструкторі класу-нащадка?
У JavaScript, коли створюється клас та використовується ключове слово extends для створення підкласу (нащадка), в конструкторі цього підкласу може бути викликано super().
Ключове слово super використовується для виклику конструктора батьківського класу. Виклик super() в конструкторі нащадка потрібен тому, що він ініціалізує створену інстанцію об'єкта у підкласі, а також виконує будь-які додаткові налаштування, які виконуються в конструкторі батьківського класу.
Крім того, якщо в батьківському класі є конструктор, і ми хочемо мати конструктор у підкласі, треба викликати super() як перший оператор у конструкторі нащадка, оскільки JavaScript вимагає, щоб конструктори були викликані відповідним чином при наслідуванні.

Отже, виклик super() у конструкторі класу-нащадка потрібен для:
1. Ініціалізації об'єкта у підкласі.
2. Виконання будь-яких додаткових операцій, що виконуються в конструкторі батьківського класу.
3. Забезпечення правильного ланцюга наслідування конструкторів.
*/

/*
Завдання
1. Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
2. Створіть гетери та сеттери для цих властивостей.
3. Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
4. Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
5. Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
    }

    set name(newName) {
        this._name = newName;
    }

    get age() {
        return this._age;
    }

    set age(newAge) {
        this._age = newAge;
    }

    get salary() {
        return this._salary;
    }

    set salary(newSalary) {
        this._salary = newSalary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    get salary() {
        return super.salary * 3;
    }

    get lang() {
        return this._lang;
    }

    set lang(newLang) {
        this._lang = newLang;
    }
}

const programmer1 = new Programmer("John", 18, 10000, "Java");
console.log(programmer1);

const programmer2 = new Programmer("Sally", 35, 15000, "Javascript");
console.log(programmer2);

console.log("Programmer 1:");
console.log("Name:", programmer1.name);
console.log("Age:", programmer1.age);
console.log("Salary:", programmer1.salary);
console.log("Languages:", programmer1.lang);
console.log();

console.log("Programmer 2:");
console.log("Name:", programmer2.name);
console.log("Age:", programmer2.age);
console.log("Salary:", programmer2.salary);
console.log("Languages:", programmer2.lang);